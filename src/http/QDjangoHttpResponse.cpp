/*
 * Copyright (C) 2010-2012 Jeremy Lainé
 * Contact: http://code.google.com/p/qdjango/
 *
 * This file is part of the QDjango Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QDebug>
#include "QDjangoHttpResponse.h"
#include "QDjangoHttpResponse_p.h"

/** Constructs a new HTTP response.
 */
QDjangoHttpResponse::QDjangoHttpResponse()
    : d(new QDjangoHttpResponsePrivate)
    {
        setHeader("Content-Length", "0");
        setStatusCode(QDjangoHttpResponse::OK);
    }

/** Destroys the HTTP response.
 */
QDjangoHttpResponse::~QDjangoHttpResponse()
    {
        delete d;
    }

/** Returns the raw body of the HTTP response.
 */
QByteArray QDjangoHttpResponse::body() const
    {
        return d->body;
    }

/** Sets the raw body of the HTTP response.
 *
 * The Content-Length header will be updated to reflect the body size.
 *
 * \param body
 */
void QDjangoHttpResponse::setBody(const QByteArray &body)
    {
        d->body = body;
        setHeader("Content-Length", QString::number(d->body.size()));
    }

/** Returns the specified HTTP response header.
 *
 * \param key
 */
QString QDjangoHttpResponse::header(const QString &key) const
    {
        return d->header.value(key);
    }

/** Sets the specified HTTP response header.
 *
 * \param key
 * \param value
 */
void QDjangoHttpResponse::setHeader(const QString &key, const QString &value)
    {
        d->header.setValue(key, value);
    }

/** Returns true if the response is ready to be sent.
 *
 * The default implementation always returns true. If you subclass
 * QDjangoHttpResponse to support responses which should only be sent
 * to the client at a later point, you need to reimplement this method
 * and emit the ready() signal once the response is ready.
 */
bool QDjangoHttpResponse::isReady() const
    {
        return true;
    }

/** Returns the code for the HTTP response status line.
 */
int QDjangoHttpResponse::statusCode() const
    {
        return d->header.statusCode();
    }

/** Sets the code for the HTTP response status line.
 *
 * \param code
 */
void QDjangoHttpResponse::setStatusCode(int code)
    {
        switch(code)
            {
        case OK:
            d->header.setStatusLine(code, "OK");
            break;
        case MovedPermanently:
            d->header.setStatusLine(code, "Moved Permanently");
            break;
        case Found:
            d->header.setStatusLine(code, "Found");
            break;
        case NotModified:
            d->header.setStatusLine(code, "Not Modified");
            break;
        case BadRequest:
            d->header.setStatusLine(code, "Bad Request");
            break;
        case AuthorizationRequired:
            d->header.setStatusLine(code, "Authorization Required");
            break;
        case Forbidden:
            d->header.setStatusLine(code, "Forbidden");
            break;
        case NotFound:
            d->header.setStatusLine(code, "Not Found");
            break;
        case MethodNotAllowed:
            d->header.setStatusLine(code, "Method Not Allowed");
            break;
        case InternalServerError:
            d->header.setStatusLine(code, "Internal Server Error");
            break;
        default:
            d->header.setStatusLine(code);
            break;
            }
    }

/** Sets a cookie for the client
 *
 * \warning If you use Chrome and localhost you have to use a hostname like local.com
 * \see http://code.google.com/p/chromium/issues/detail?id=56211
 *
 * \param name
 * \param value
 * \param maxAge  [optional] In seconds
 * \param domain  [optional]
 * \param path    [optional]
 * \param version [optional]
 * \param secure  [optional] If it only should be sent during an ssl connection
 * \param comment [optional] Document what is in the cookie
 */
void QDjangoHttpResponse::setCookie(const QString &name, const QString &value, const QDateTime expires, const QString &domain, const QString &path, HttpCookieOption option)
    {
        QString cookieData = "";
        const QString seperator = "; ";
        const QString equals = "=";

        cookieData += name + equals + value + seperator;

        if(!expires.isNull())
            cookieData += QString("expires") + equals + expires.toString("ddd, dd-MMM-yyyy hh:mm:ss") + QString(" GMT") + seperator;

        if(!path.isEmpty())
            cookieData += QString("path") + equals + path + seperator;

        if(!domain.isEmpty())
            cookieData += QString("domain") + equals + domain + seperator;

        if(option != QDjangoHttpResponse::NoCookieOption)
            {
                if(option & QDjangoHttpResponse::SecureCookieOption)
                    cookieData += QString("secure") + seperator;

                if(option & QDjangoHttpResponse::HttpOnlyCookieOption)
                    cookieData += QString("httponly") + seperator;
            }

        cookieData.left(cookieData.size() - seperator.size());

        d->header.setValue("Set-Cookie", cookieData);
    }
