/*
 * Copyright (C) 2012 Sascha Häusler
 * Contact:
 *
 * This file is part of the QDjango Library.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QSSLTCPSERVER_H
#define QSSLTCPSERVER_H

#include <QTcpServer>
#include <QSslSocket>

class QSslTcpServerPrivate;

class QSslTcpServer : public QTcpServer
{
        Q_OBJECT
    public:
        explicit QSslTcpServer(QObject *parent = 0);
        ~QSslTcpServer();

        void setCaCertificate(const QSslCertificate & certificate );
        void setPrivateKey ( const QString & fileName, QSsl::KeyAlgorithm algorithm = QSsl::Rsa, QSsl::EncodingFormat format = QSsl::Pem, const QByteArray & passPhrase = QByteArray() );

        QSslSocket* nextPendingSslConnection();

    public slots:
        void ready();

    signals:
        void newSecureConnection();

    protected:
        virtual void incomingConnection(int handle);

    private:
        QSslTcpServerPrivate* d;
        
};

#endif // QSSLTCPSERVER_H
