#ifndef QDJANGOSECUREHTTPSERVER_H
#define QDJANGOSECUREHTTPSERVER_H

#include <QSslSocket>
#include "QDjangoHttpServer.h"

class QDjangoSecureHttpServerPrivate;

class QDJANGO_EXPORT QDjangoSecureHttpServer : public QDjangoHttpServer
{
        Q_OBJECT
    public:
        QDjangoSecureHttpServer();
        virtual ~QDjangoSecureHttpServer();

        virtual void close();
        virtual bool listen(const QHostAddress &address, quint16 port);

        void setCaCertificate(const QSslCertificate & certificate );
        void setPrivateKey ( const QString & fileName, QSsl::KeyAlgorithm algorithm = QSsl::Rsa, QSsl::EncodingFormat format = QSsl::Pem, const QByteArray & passPhrase = QByteArray() );

    private slots:
        void _q_newSecureTcpConnection();

    private:
        Q_DISABLE_COPY(QDjangoSecureHttpServer)
        QDjangoSecureHttpServerPrivate* const d;
};

#endif // QDJANGOSECUREHTTPSERVER_H
