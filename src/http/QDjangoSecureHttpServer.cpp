#include "QSslTcpServer.h"
#include "QDjangoSecureHttpServer.h"
#include "QDjangoHttpServer_p.h"

class QDjangoSecureHttpServerPrivate
{
    public:
        QSslTcpServer* sslTcpServer;
};


QDjangoSecureHttpServer::QDjangoSecureHttpServer() :
    d(new QDjangoSecureHttpServerPrivate)
    {
        d->sslTcpServer = 0;
    }

QDjangoSecureHttpServer::~QDjangoSecureHttpServer()
    {
        delete d;
    }

/** Closes the server. The server will no longer listen for
 *  incoming connections.
 */
void QDjangoSecureHttpServer::close()
    {
        if (d->sslTcpServer)
            d->sslTcpServer->close();
    }

/** Tells the server to listen for incoming TCP connections on the given
 *  \a address and \a port.
 */
bool QDjangoSecureHttpServer::listen(const QHostAddress &address, quint16 port)
    {
        if (!d->sslTcpServer) {
                bool check;
                Q_UNUSED(check);

                d->sslTcpServer = new QSslTcpServer(this);
                check = connect(d->sslTcpServer, SIGNAL(newSecureConnection()),
                                this, SLOT(_q_newSecureTcpConnection())
                                );
                Q_ASSERT(check);
            }

        return d->sslTcpServer->listen(address, port);
    }

void QDjangoSecureHttpServer::setCaCertificate(const QSslCertificate &certificate)
    {
        d->sslTcpServer->setCaCertificate(certificate);
    }

void QDjangoSecureHttpServer::setPrivateKey(const QString &fileName, QSsl::KeyAlgorithm algorithm, QSsl::EncodingFormat format, const QByteArray &passPhrase)
    {
        d->sslTcpServer->setPrivateKey(fileName, algorithm, format, passPhrase);
    }

void QDjangoSecureHttpServer::_q_newSecureTcpConnection()
    {
        bool check;
        Q_UNUSED(check);

        QSslSocket *socket;
        while ((socket = d->sslTcpServer->nextPendingSslConnection()) != 0) {
                QDjangoHttpConnection *connection = new QDjangoHttpConnection(socket, this);

                check = connect(connection, SIGNAL(closed()),
                                connection, SLOT(deleteLater()));
                Q_ASSERT(check);

                check = connect(connection, SIGNAL(requestFinished(QDjangoHttpRequest*,QDjangoHttpResponse*)),
                                this, SIGNAL(requestFinished(QDjangoHttpRequest*,QDjangoHttpResponse*)));
                Q_ASSERT(check);
            }
    }
