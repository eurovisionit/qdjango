#include <QDebug>
#include <QtCore/QFile>
#include <QtCore/QQueue>
#include <QtNetwork/QSslKey>
#include "QSslTcpServer.h"

class QSslTcpServerPrivate
{
    public:
        QSslCertificate certificate;
        QSslKey sslKey;
        QQueue<QSslSocket *> connectionQueue;
};

QSslTcpServer::QSslTcpServer(QObject *parent) :
    QTcpServer(parent),
    d(new QSslTcpServerPrivate)
    {
    }

QSslTcpServer::~QSslTcpServer()
    {
        delete d;
    }

void QSslTcpServer::setCaCertificate(const QSslCertificate &certificate)
    {
        d->certificate = certificate;
    }

void QSslTcpServer::setPrivateKey(const QString &fileName, QSsl::KeyAlgorithm algorithm, QSsl::EncodingFormat format, const QByteArray &passPhrase)
    {
        QFile file(fileName);
        if(!file.open(QIODevice::ReadOnly))
            return;

        QByteArray fileData = file.readAll();

        file.close();

        d->sslKey = QSslKey(fileData, algorithm, format, QSsl::PrivateKey, passPhrase);
    }

QSslSocket* QSslTcpServer::nextPendingSslConnection()
    {
        return (d->connectionQueue.isEmpty()) ? 0 : d->connectionQueue.dequeue();
    }

void QSslTcpServer::ready()
    {
        emit newSecureConnection();
    }

void QSslTcpServer::incomingConnection(int handle)
    {
        QSslSocket *serverSocket = new QSslSocket;
        serverSocket->setProtocol(QSsl::TlsV1SslV3);
        serverSocket->setLocalCertificate(d->certificate);
        serverSocket->setPrivateKey(d->sslKey);

        if (serverSocket->setSocketDescriptor(handle)) {

                connect(serverSocket, SIGNAL(encrypted()), this, SLOT(ready()));

                serverSocket->startServerEncryption();
                d->connectionQueue.enqueue(serverSocket);

            } else {
                delete serverSocket;
            }
    }
